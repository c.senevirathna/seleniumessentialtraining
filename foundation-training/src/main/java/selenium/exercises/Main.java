package selenium.exercises;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main {
    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver","C://Users//User//OneDrive//Documents//chromedriver-win64//chromedriver-win64//chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        BookStoreApplication bookStoreApplication = new BookStoreApplication(webDriver);
        bookStoreApplication.navigateToBookStoreApp("https://demoqa.com/");
    }
}
