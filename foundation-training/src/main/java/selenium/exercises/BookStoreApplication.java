package selenium.exercises;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import java.util.concurrent.TimeUnit;

public class BookStoreApplication {
    WebDriver driver;

    public BookStoreApplication(WebDriver webDriver) {
        this.driver = webDriver;
    }

    public void navigateToBookStoreApp(String url){
        Actions actions = new Actions(driver);
        driver.navigate().to(url);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        System.out.println("waiting timeout");
        actions.moveToElement(driver.findElement(By.xpath("//*[@class='category-cards']//child::div[6]/div//child::div[3]/h5"))).click().perform();
        System.out.println("object found");
        WebElement webElement = driver.findElement(By.xpath("//*[@class='category-cards']//child::div[6]/div//child::div[3]/h5"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
        webElement.click();
    }
}
