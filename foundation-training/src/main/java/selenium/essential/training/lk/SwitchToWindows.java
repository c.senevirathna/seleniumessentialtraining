package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SwitchToWindows {

    public void SwitchToWindows(WebDriver driver){
        String originalHandle = driver.getWindowHandle();
        driver.get("https://formy-project.herokuapp.com/switch-window");
        driver.findElement(By.id("new-tab-button")).click();

        for(String handle1: driver.getWindowHandles()){
            driver.switchTo().window(handle1);
        }
        driver.switchTo().window(originalHandle);
    }
}
