package selenium.essential.training.lk;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SwitchToAlertBox {
    public void SwitchToAlert(WebDriver driver){
        driver.get("https://formy-project.herokuapp.com/switch-window");
        driver.findElement(By.id("alert-button")).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }
}
