package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class ImplicitWaits {
    public void ImplicitWaitFeature(WebDriver driver){
        driver.get("https://formy-project.herokuapp.com/scroll");
        System.out.println("implicitly waiting");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);
        System.out.println("Waiting time over");
        driver.findElement(By.id("logo")).click();
    }
}
