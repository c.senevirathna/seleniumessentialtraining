package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;

public class PageScroll {

    public void ScrollToElement(WebDriver driver){
        Actions actions = new Actions(driver);
        driver.get("https://formy-project.herokuapp.com/scroll");
        actions.moveToElement(driver.findElement(By.id("name"))).sendKeys("Amina Patel");
        driver.findElement(By.id("date")).sendKeys("02/12/2023");
    }
}
