package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Driver;

public class KeyboardAndMouseInput {
    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C://Users//User//OneDrive//Documents//chromedriver-win64//chromedriver-win64//chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("https://formy-project.herokuapp.com/keypress");
        driver.findElement(By.id("name")).sendKeys("Megan Lewi");
        driver.findElement(By.id("button")).click();

        /*AutoComplete autoComplete = new AutoComplete();
        autoComplete.AutoCompleteFuctionality(driver);*/

        PageScroll pageScroll = new PageScroll();
        pageScroll.ScrollToElement(driver);

        SwitchToWindows switchToWindows = new SwitchToWindows();
        switchToWindows.SwitchToWindows(driver);

        SwitchToAlertBox switchToAlertBox = new SwitchToAlertBox();
        switchToAlertBox.SwitchToAlert(driver);

        /*JavaScriptCommands javaScriptCommands = new JavaScriptCommands();
        javaScriptCommands.JavaScriptCommandsHandling(driver);*/

        RadioButtons radioButtons = new RadioButtons();
        radioButtons.RadioButtonFunc(driver);

        DayPicker dayPicker = new DayPicker();
        dayPicker.DayPickerFunc(driver);

        Dropdown dropdown = new Dropdown();
        dropdown.DropdownFunc(driver);

        FileUpload fileUpload = new FileUpload();
        fileUpload.fileUpload(driver);

        ImplicitWaits implicitWaits = new ImplicitWaits();
        implicitWaits.ImplicitWaitFeature(driver);

        ExplicitWait explicitWait = new ExplicitWait();
        explicitWait.ExplicitlyWaiting(driver);

        CompleteWebForm completeWebForm = new CompleteWebForm();
        completeWebForm.WebForm(driver);
        driver.quit();
    }
}