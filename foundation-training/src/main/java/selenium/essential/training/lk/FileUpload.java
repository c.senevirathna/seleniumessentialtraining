package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FileUpload {
    public void fileUpload(WebDriver driver){
        driver.get("https://formy-project.herokuapp.com/fileupload");
        WebElement fileUploadField = driver.findElement(By.id("file-upload-field"));
        fileUploadField.sendKeys("fileupload.png");
        driver.findElement(By.className("btn-reset")).click();
    }
}
