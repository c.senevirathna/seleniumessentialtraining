package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JavaScriptCommands {
    public void JavaScriptCommandsHandling(WebDriver driver){
        driver.get("https://formy-project.herokuapp.com/modal");
        driver.findElement(By.id("modal-button")).click();
        WebElement closeButton = driver.findElement(By.id("close-button"));
        closeButton.click();
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("arguments[0].click()",closeButton);
    }
}
