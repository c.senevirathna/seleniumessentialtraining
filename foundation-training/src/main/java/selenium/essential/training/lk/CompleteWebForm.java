package selenium.essential.training.lk;

import dev.failsafe.internal.util.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CompleteWebForm {
    public void WebForm(WebDriver driver) throws InterruptedException {
        driver.get("https://formy-project.herokuapp.com/form");

        driver.findElement(By.id("first-name")).sendKeys("Kylie");
        driver.findElement(By.id("last-name")).sendKeys("Angel");
        driver.findElement(By.id("job-title")).sendKeys("QA Engineer");
        driver.findElement(By.id("radio-button-3")).click();
        driver.findElement(By.id("checkbox-2")).click();
        driver.findElement(By.id("select-menu")).click();
        driver.findElement(By.xpath("//option[@value='4']")).click();
        driver.findElement(By.id("datepicker")).sendKeys("02/12/2003");
        driver.findElement(By.className("btn-primary")).click();

        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(15));
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-success")));

        Thread.sleep(20000);
    }
}
