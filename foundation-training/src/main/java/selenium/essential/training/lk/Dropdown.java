package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Dropdown {
    public void DropdownFunc(WebDriver driverb){
        driverb.get("https://formy-project.herokuapp.com/dropdown");
        WebElement ddd = driverb.findElement(By.id("dropdownMenuButton"));
        ddd.click();
        WebElement ddValues = driverb.findElement(By.id("autocomplete"));
        ddValues.click();
    }
}
