package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DayPicker {
    public void DayPickerFunc(WebDriver driver){
        driver.get("https://formy-project.herokuapp.com/datepicker");
        WebElement dayPick = driver.findElement(By.id("datepicker"));
        dayPick.sendKeys("03/03/2023");
        dayPick.sendKeys(Keys.RETURN);

    }
}
