package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RadioButtons {
    public void RadioButtonFunc(WebDriver driver){

        driver.get("https://formy-project.herokuapp.com/radiobutton");
        driver.findElement(By.id("radio-button-1")).click();
        driver.findElement(By.cssSelector("input[value='option2']")).click();
        driver.findElement(By.xpath("//input[@name = 'exampleRadios']")).click();
    }
}
