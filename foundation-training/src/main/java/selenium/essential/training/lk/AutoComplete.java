package selenium.essential.training.lk;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.sql.Driver;

public class AutoComplete {
    public void AutoCompleteFuctionality(WebDriver driver) throws InterruptedException {
        driver.get("https://formy-project.herokuapp.com/autocomplete");
        driver.findElement(By.id("autocomplete")).sendKeys("1555 Park Blvd, Palo Alto, CA");
        Thread.sleep(1000);
        driver.findElement(By.className("pac-item")).click();
    }
}
